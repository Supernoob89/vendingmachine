package com.kcom.vendingmachine;

import java.util.Collection;

import com.kcom.vendingmachine.exceptions.InsufficientCoinageException;
import com.kcom.vendingmachine.exceptions.InvalidAmountException;

/**
 * Allows the ability to get the suitable change in {@link CoinDenomination}s for a
 * requested amount.
 * 
 * @author Luca
 *
 */
public interface ChangeMaker {

  /**
   *
   * @param pence
   * @return The optimal/fewest possible coins that make up the submitted value.
   */
  public Collection<Coin> getOptimalChangeFor(int pence);

  /**
   * 
   * @param pence
   * @return The optimal/fewest possible coins out of what coins are available
   *         in the store.
   * @throws InsufficientCoinageException
   */
  public Collection<Coin> getChangeFor(int pence)
      throws InsufficientCoinageException;
}
