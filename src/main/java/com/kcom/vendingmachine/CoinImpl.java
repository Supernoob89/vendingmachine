package com.kcom.vendingmachine;

public class CoinImpl implements Coin {

  private final CoinDenomination coinDenomination;
  private final int amountOfCoins;

  public CoinImpl(CoinDenomination coinDenomination, int amountOfCoins) {
    this.coinDenomination = coinDenomination;
    this.amountOfCoins = amountOfCoins;
  }

  @Override
  public int getDenomination() {
    return coinDenomination.getDenomination();
  }

  @Override
  public int getAmountOfCoins() {
    return amountOfCoins;
  }

}
