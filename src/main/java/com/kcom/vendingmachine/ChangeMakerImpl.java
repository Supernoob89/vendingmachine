package com.kcom.vendingmachine;

import java.util.Collection;

import com.kcom.vendingmachine.exceptions.InsufficientCoinageException;
import com.kcom.vendingmachine.exceptions.InvalidAmountException;

public class ChangeMakerImpl implements ChangeMaker {

  private CoinManager coinManager;

  public ChangeMakerImpl(CoinStore store) {
    coinManager = new CoinManager(store);
  }

  public Collection<Coin> getOptimalChangeFor(int pence) {
      return coinManager.createOptimumCoins(pence);
  }

  public Collection<Coin> getChangeFor(int pence) throws InsufficientCoinageException {
      return coinManager.createCoinsWithStore(pence);
  }

}
