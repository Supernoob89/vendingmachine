package com.kcom.vendingmachine;

public interface Coin {

  public int getDenomination();

  public int getAmountOfCoins();
}
