package com.kcom.vendingmachine;

/**
 * Determines how many coins can be created based on the store information.
 * 
 * @author Luca
 *
 */
public class StoreBasedCoinageDeterminer implements CoinageDeterminer {

  private final CoinStore coinageStore;

  public StoreBasedCoinageDeterminer(CoinStore store) {
    coinageStore = store;
  }

  /**
   * Will provide as close to optimum amount of coins, depending on what is
   * available in the store.
   */
  @Override
  public int determineAmountOfCoins(int pence, CoinDenomination coin) {
    int amountOfCoins = BasicCoinageDeterminerLogic.pDetermineAmountOfCoins(
        pence, coin);
    int coinAvailability = coinageStore.findCoinAvailability(coin);
    return coinAvailability < amountOfCoins ? coinAvailability : amountOfCoins;
  }

}
