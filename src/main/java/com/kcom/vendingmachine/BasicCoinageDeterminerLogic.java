package com.kcom.vendingmachine;

/**
 * Basic functions to compute the required coins.
 * 
 * @author Luca
 *
 */
public class BasicCoinageDeterminerLogic {

  /**
   * Determines how many of a certain coin can be attained from the numeric
   * value provided.
   * 
   * @param pence
   * @param coin
   * @return
   */
  public static int pDetermineAmountOfCoins(int pence, CoinDenomination coin) {
    return pence / coin.getDenomination();
  }

}
