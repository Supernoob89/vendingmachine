package com.kcom.vendingmachine;

/**
 * Represention of the possible coins to be used.
 *
 * @author Luca
 *
 */
public enum CoinDenomination {

  ONE_PENCE(1),
  ONE_POUND(100), 
  FIFTY_PENCE(50), 
  TWENTY_PENCE(20), 
  TEN_PENCE(10), 
  FIVE_PENCE(5),
  TWO_PENCE(2);

  private int denomination;

  private CoinDenomination(int denomination) {
    this.denomination = denomination;
  }

  public int getDenomination() {
    return denomination;
  }

}
