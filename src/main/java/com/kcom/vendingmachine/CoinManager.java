package com.kcom.vendingmachine;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.kcom.vendingmachine.exceptions.InsufficientCoinageException;

/**
 * Manages the creation of any coins.
 *
 * @author Luca
 *
 */
public class CoinManager {

  private static final List<CoinDenomination> COIN_DENOMINATIONS = Arrays
      .asList(CoinDenomination.values());
  {
    COIN_DENOMINATIONS.sort((t1, t2) -> t2.getDenomination() < t1
        .getDenomination() ? -1
        : t2.getDenomination() > t1.getDenomination() ? 1 : 0);
  }

  private final CoinStore coinStore;

  public CoinManager(CoinStore coinStore) {
    this.coinStore = coinStore;
  }

  /**
   * Creates necessary coinage as long as there is sufficient stocks of coins in
   * the store.
   * 
   * @param pence
   * @throws InsufficientCoinageException
   *           If not enough available coins.
   */
  public Collection<Coin> createCoinsWithStore(int pence)
      throws InsufficientCoinageException {
    Collection<Coin> coinCollection = new ArrayList<>();
    Map<CoinDenomination, Integer> coins = getCoins(pence,
        new StoreBasedCoinageDeterminer(coinStore));

    checkIfCorrectCoinage(pence, coins);

    coins.forEach((coin, amount) -> {
      if (amount > 0) {
        coinCollection.add(new CoinImpl(coin, amount));
        coinStore.removeCoins(coin, amount);
      }
    });
    return coinCollection;
  }

  /**
   * Creates necessary coinage always.
   * 
   * @param pence
   */
  public Collection<Coin> createOptimumCoins(int pence) {
    Collection<Coin> coinCollection = new ArrayList<>();
    Map<CoinDenomination, Integer> coins = getCoins(pence,
        BasicCoinageDeterminerLogic::pDetermineAmountOfCoins);
    coins.forEach((coin, amount) -> {
      if (amount > 0) {
        coinCollection.add(new CoinImpl(coin, amount));
      }
    });
    return coinCollection;
  }

  private Map<CoinDenomination, Integer> getCoins(int pence,
      CoinageDeterminer coinageDeterminer) {
    Map<CoinDenomination, Integer> coins = new HashMap<>();
    for (CoinDenomination coin : COIN_DENOMINATIONS) {
      if (pence < 1) {
        break;
      }
      int amountOfCoins = coinageDeterminer.determineAmountOfCoins(pence, coin);
      pence = pence - (amountOfCoins * coin.getDenomination());

      coins.put(coin, amountOfCoins);
    }
    return coins;
  }

  private void checkIfCorrectCoinage(int pence,
      Map<CoinDenomination, Integer> coins) throws InsufficientCoinageException {
    int coinsTotal = 0;
    for (CoinDenomination coin : coins.keySet()) {
      coinsTotal = coinsTotal + (coin.getDenomination() * coins.get(coin));
    }

    if (coinsTotal < pence) {
      throw new InsufficientCoinageException(
          "Insufficient coinage for requested amount");
    }
  }
}
