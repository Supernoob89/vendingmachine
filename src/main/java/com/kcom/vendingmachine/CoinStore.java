package com.kcom.vendingmachine;

/**
 * Manages an inventory of available coins.
 * 
 * @author Luca
 *
 */
public interface CoinStore {

  /**
   * Check coin availability
   * @param coin
   * @return How many of the provided {@link CoinDenomination} are available
   */
  public int findCoinAvailability(CoinDenomination coin);

  /**
   * Decreases the value by the provided amount for the provided {@link CoinDenomination}s
   * 
   * @param coin
   * @param newAvailability
   */
  public void removeCoins(CoinDenomination coin, int amountToRemove);
}
