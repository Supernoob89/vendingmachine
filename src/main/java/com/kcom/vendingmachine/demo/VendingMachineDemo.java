package com.kcom.vendingmachine.demo;

import java.util.Collection;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;

import com.kcom.vendingmachine.ChangeMaker;
import com.kcom.vendingmachine.ChangeMakerImpl;
import com.kcom.vendingmachine.Coin;
import com.kcom.vendingmachine.CoinDenomination;
import com.kcom.vendingmachine.PropertiesCoinStore;
import com.kcom.vendingmachine.exceptions.FailedToLoadPropertiesException;
import com.kcom.vendingmachine.exceptions.InsufficientCoinageException;
import com.kcom.vendingmachine.exceptions.InvalidAmountException;

public class VendingMachineDemo {

  private final Scanner scanner = new Scanner(System.in);
  private ChangeMaker changeMaker = null;

  public void runDemo() {
    while (true) {
      if (changeMaker == null) {
        promptForPropertiesPath();
      } else {
        promptForPenceInput();
      }
    }
  }

  private void promptForPenceInput() {
    System.out
        .println("Please press 1 for optimal change or 2 for store based change");
    String number = scanner.nextLine();

    switch (number) {

    case "1":
      System.out.println("Please enter number you would like change for:");
      try {
        Collection<Coin> coins = changeMaker.getOptimalChangeFor(Integer
            .parseInt(scanner.nextLine()));
        printOutResultingCoins(coins);
      } catch (NumberFormatException e) {
        System.out.println("Please make sure you provide a number");
      }
      break;

    case "2":
      System.out.println("Please enter number you would like change for:");
      try {
        Collection<Coin> coins = changeMaker.getChangeFor(Integer
            .parseInt(scanner.nextLine()));
        printOutResultingCoins(coins);
      } catch (NumberFormatException e) {
        System.out.println("Please make sure you provide a number");
      } catch (InsufficientCoinageException insuff) {
        System.out.println("Sorry there are not enough coins for your request");
        insuff.printStackTrace();
      }
      break;
    }
  }

  private void promptForPropertiesPath() {
    System.out.println("");
    System.out
        .println("Please enter directory path of coin-inventory.properties");
    try {
      tryCreateChangeMaker(scanner.nextLine());
    } catch (FailedToLoadPropertiesException e) {
      System.out
          .println("Could not load properties file exception was thrown\n");
      e.printStackTrace();
    }

  }

  private void tryCreateChangeMaker(String directoryPath)
      throws FailedToLoadPropertiesException {
    changeMaker = new ChangeMakerImpl(new PropertiesCoinStore(directoryPath));
  }

  private void printOutResultingCoins(Collection<Coin> coins) {
    coins.forEach((coin) -> System.out.println("Coin "
        + coin.getDenomination() + " : " + coin.getAmountOfCoins()));
  }
}
