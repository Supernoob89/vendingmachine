package com.kcom.vendingmachine;

/**
 * Works out the available coins for a particular denomination.
 * 
 * @author Luca
 *
 */
public interface CoinageDeterminer {

  /**
   * 
   * @param pence
   * @param coin
   * @return To work out how many of a certain coin can be given for the provided amount.
   */
  public int determineAmountOfCoins(int pence, CoinDenomination coin);

}
