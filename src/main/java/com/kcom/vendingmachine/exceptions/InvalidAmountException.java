package com.kcom.vendingmachine.exceptions;

/**
 * An illegal amount was attempted to be submitted.
 *
 * @author Luca
 *
 */
public class InvalidAmountException extends Exception {

  public InvalidAmountException(String errorMessage) {
    super(errorMessage);
  }
}
