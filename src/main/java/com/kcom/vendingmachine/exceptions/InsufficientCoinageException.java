package com.kcom.vendingmachine.exceptions;

/**
 * Represents if the vending machine does not have adequate amounts of coins to
 * fulfil a request.
 * 
 * @author Luca
 *
 */
public class InsufficientCoinageException extends Exception {

  public InsufficientCoinageException(String errorMessage) {
    super(errorMessage);
  }
}
