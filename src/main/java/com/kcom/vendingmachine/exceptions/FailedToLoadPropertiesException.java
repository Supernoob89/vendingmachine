package com.kcom.vendingmachine.exceptions;

/**
 * Representation of not being able to find/load a properties file.
 * @author Luca
 *
 */
public class FailedToLoadPropertiesException extends Exception {

  public FailedToLoadPropertiesException(String message) {
    super(message);
  }

  public FailedToLoadPropertiesException(String message, Throwable throwable) {
    super(message, throwable);
  }
}
