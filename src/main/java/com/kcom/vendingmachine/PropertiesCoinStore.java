package com.kcom.vendingmachine;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import com.kcom.vendingmachine.exceptions.FailedToLoadPropertiesException;

/**
 * Properties base implementation of a {@link CoinStore}
 * 
 * @author Luca
 *
 */
public class PropertiesCoinStore implements CoinStore {

  private static final String PROPERTIES_FILE_NAME = "coin-inventory.properties";
  private final Properties coinStore;

  /**
   * 
   * @param propertiesDirectory
   *          The path of the directory which contains
   *          "coin-inventory.properties".
   * @throws FailedToLoadPropertiesException
   */
  public PropertiesCoinStore(String propertiesDirectory)
      throws FailedToLoadPropertiesException {
    coinStore = new Properties();
    try {
      FileInputStream fileInputStream = new FileInputStream(propertiesDirectory
          + "/" + PROPERTIES_FILE_NAME);
      coinStore.load(fileInputStream);
    } catch (FileNotFoundException fileNotFound) {
      throw new FailedToLoadPropertiesException("Cannot find properties file "
          + PROPERTIES_FILE_NAME + " at location - " + propertiesDirectory,
          fileNotFound);
    } catch (IOException e) {
      throw new FailedToLoadPropertiesException(
          "An error occurred when trying to load " + PROPERTIES_FILE_NAME, e);
    }
  }

  public PropertiesCoinStore(Properties properties) {
    coinStore = properties;
  }

  public int findCoinAvailability(CoinDenomination coin) {
    String stringNumber = coinStore.getProperty(Integer.toString(coin
        .getDenomination()));
    if (stringNumber == null) {
      return 0;
    }
    return Integer.parseInt(stringNumber);
  }

  public void removeCoins(CoinDenomination coin, int numberOfCoins) {
    int original = findCoinAvailability(coin);
    coinStore.setProperty(Integer.toString(coin.getDenomination()),
        Integer.toString(original - numberOfCoins));
  }
}
