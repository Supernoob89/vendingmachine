package com.kcom.vendingmachine;

import static org.junit.Assert.*;

import java.util.Properties;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.kcom.vendingmachine.CoinDenomination;
import com.kcom.vendingmachine.PropertiesCoinStore;
import com.kcom.vendingmachine.exceptions.FailedToLoadPropertiesException;

public class PropertiesCoinStoreTest {

  @Rule
  public ExpectedException exception = ExpectedException.none();

  @Test
  public void testFilePathNotFound() throws Exception {
    exception.expect(FailedToLoadPropertiesException.class);
    exception
        .expectMessage("Cannot find properties file coin-inventory.properties at location - random/path/to/nowhere");
    new PropertiesCoinStore("random/path/to/nowhere");
  }

  @Test(expected = FailedToLoadPropertiesException.class)
  public void testNullFilePath() throws Exception {
    new PropertiesCoinStore((String) null);
  }

  @Test
  public void correctPath() throws Exception {
    assertEquals(234,
        new PropertiesCoinStore("src/test/resources/")
            .findCoinAvailability(CoinDenomination.ONE_POUND));
  }

  @Test
  public void missingTrailingForwardSlash() throws Exception {
    assertEquals(234,
        new PropertiesCoinStore("src/test/resources")
            .findCoinAvailability(CoinDenomination.ONE_POUND));
  }

  @Test
  public void findOnePoundCoins() {
    Properties properties = new Properties();
    properties.put("100", "2345");
    assertEquals(2345,
        new PropertiesCoinStore(properties)
            .findCoinAvailability(CoinDenomination.ONE_POUND));
  }

  @Test
  public void removeOnePoundCoins() {
    Properties properties = new Properties();
    properties.put("100", "2345");
    PropertiesCoinStore store = new PropertiesCoinStore(properties);
    store.removeCoins(CoinDenomination.ONE_POUND, 5);
    assertEquals("2340", properties.getProperty("100"));
  }

  @Test
  public void findFiftyPenceCoins() {
    Properties properties = new Properties();
    properties.put("50", "2345");
    assertEquals(2345,
        new PropertiesCoinStore(properties)
            .findCoinAvailability(CoinDenomination.FIFTY_PENCE));
  }

  @Test
  public void removeFiftyPenceCoins() {
    Properties properties = new Properties();
    properties.put("50", "2345");
    PropertiesCoinStore store = new PropertiesCoinStore(properties);
    store.removeCoins(CoinDenomination.FIFTY_PENCE, 5);
    assertEquals("2340", properties.getProperty("50"));
  }

  @Test
  public void findTwentyPenceCoins() {
    Properties properties = new Properties();
    properties.put("20", "2345");
    assertEquals(2345,
        new PropertiesCoinStore(properties)
            .findCoinAvailability(CoinDenomination.TWENTY_PENCE));
  }

  @Test
  public void removeTwentyPenceCoins() {
    Properties properties = new Properties();
    properties.put("20", "2345");
    PropertiesCoinStore store = new PropertiesCoinStore(properties);
    store.removeCoins(CoinDenomination.TWENTY_PENCE, 5);
    assertEquals("2340", properties.getProperty("20"));
  }

  @Test
  public void findTenPenceCoins() {
    Properties properties = new Properties();
    properties.put("10", "2345");
    assertEquals(2345,
        new PropertiesCoinStore(properties)
            .findCoinAvailability(CoinDenomination.TEN_PENCE));
  }

  @Test
  public void removeTenPenceCoins() {
    Properties properties = new Properties();
    properties.put("10", "2345");
    PropertiesCoinStore store = new PropertiesCoinStore(properties);
    store.removeCoins(CoinDenomination.TEN_PENCE, 5);
    assertEquals("2340", properties.getProperty("10"));
  }

  @Test
  public void findFivePenceCoins() {
    Properties properties = new Properties();
    properties.put("5", "2345");
    assertEquals(2345,
        new PropertiesCoinStore(properties)
            .findCoinAvailability(CoinDenomination.FIVE_PENCE));
  }

  @Test
  public void removeFivePenceCoins() {
    Properties properties = new Properties();
    properties.put("5", "2345");
    PropertiesCoinStore store = new PropertiesCoinStore(properties);
    store.removeCoins(CoinDenomination.FIVE_PENCE, 5);
    assertEquals("2340", properties.getProperty("5"));
  }

  @Test
  public void findTwoPenceCoins() {
    Properties properties = new Properties();
    properties.put("2", "2345");
    assertEquals(2345,
        new PropertiesCoinStore(properties)
            .findCoinAvailability(CoinDenomination.TWO_PENCE));
  }

  @Test
  public void removeTwoPenceCoins() {
    Properties properties = new Properties();
    properties.put("2", "2345");
    PropertiesCoinStore store = new PropertiesCoinStore(properties);
    store.removeCoins(CoinDenomination.TWO_PENCE, 5);
    assertEquals("2340", properties.getProperty("2"));
  }

  @Test
  public void findOnePenceCoins() {
    Properties properties = new Properties();
    properties.put("1", "2345");
    assertEquals(2345,
        new PropertiesCoinStore(properties)
            .findCoinAvailability(CoinDenomination.ONE_PENCE));
  }

  @Test
  public void removeOnePenceCoins() {
    Properties properties = new Properties();
    properties.put("1", "2345");
    PropertiesCoinStore store = new PropertiesCoinStore(properties);
    store.removeCoins(CoinDenomination.ONE_PENCE, 5);
    assertEquals("2340", properties.getProperty("1"));
  }
}
