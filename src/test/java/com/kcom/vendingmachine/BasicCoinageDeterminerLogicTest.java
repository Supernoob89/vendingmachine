package com.kcom.vendingmachine;

import static org.junit.Assert.*;

import org.junit.Test;

import com.kcom.vendingmachine.BasicCoinageDeterminerLogic;
import com.kcom.vendingmachine.CoinDenomination;

public class BasicCoinageDeterminerLogicTest {

  @Test
  public void roundDownFromSmallestOne() {
    assertEquals(2,
        BasicCoinageDeterminerLogic.pDetermineAmountOfCoins(21, CoinDenomination.TEN_PENCE));
  }

  @Test
  public void roundDownFromLargestOne() {
    assertEquals(2,
        BasicCoinageDeterminerLogic.pDetermineAmountOfCoins(29, CoinDenomination.TEN_PENCE));
  }

  @Test
  public void coinsForZeroPence() {
    assertEquals(0,
        BasicCoinageDeterminerLogic.pDetermineAmountOfCoins(0, CoinDenomination.TEN_PENCE));
  }
}
