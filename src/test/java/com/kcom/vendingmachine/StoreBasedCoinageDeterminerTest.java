package com.kcom.vendingmachine;

import static org.junit.Assert.*;

import java.util.Properties;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.kcom.vendingmachine.CoinDenomination;
import com.kcom.vendingmachine.PropertiesCoinStore;
import com.kcom.vendingmachine.StoreBasedCoinageDeterminer;

public class StoreBasedCoinageDeterminerTest {

  private Properties properties;

  @Before
  public void setup() {
    properties = Mockito.mock(Properties.class);
  }

  @Test
  public void emptyStore() {
    StoreBasedCoinageDeterminer determiner = new StoreBasedCoinageDeterminer(
        new PropertiesCoinStore(properties));
    assertEquals(0, determiner.determineAmountOfCoins(1, CoinDenomination.ONE_PENCE));
  }

  @Test
  public void limitedCoinsAvailableStore() {
    Mockito.when(properties.getProperty("1")).thenReturn(Integer.toString(5));
    StoreBasedCoinageDeterminer determiner = new StoreBasedCoinageDeterminer(
        new PropertiesCoinStore(properties));
    assertEquals(5, determiner.determineAmountOfCoins(6, CoinDenomination.ONE_PENCE));
  }

  @Test
  public void justEnoughCoinsAvailable() {
    Mockito.when(properties.getProperty("1")).thenReturn(Integer.toString(11));
    StoreBasedCoinageDeterminer determiner = new StoreBasedCoinageDeterminer(
        new PropertiesCoinStore(properties));
    assertEquals(11, determiner.determineAmountOfCoins(11, CoinDenomination.ONE_PENCE));
  }

  @Test
  public void excessiveCoinsAvailable() {
    Mockito.when(properties.getProperty("1")).thenReturn(Integer.toString(40));
    StoreBasedCoinageDeterminer determiner = new StoreBasedCoinageDeterminer(
        new PropertiesCoinStore(properties));
    assertEquals(11, determiner.determineAmountOfCoins(11, CoinDenomination.ONE_PENCE));
  }

}
