package com.kcom.vendingmachine;

import static org.junit.Assert.*;

import java.util.Collection;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.kcom.vendingmachine.ChangeMaker;
import com.kcom.vendingmachine.ChangeMakerImpl;
import com.kcom.vendingmachine.CoinDenomination;
import com.kcom.vendingmachine.PropertiesCoinStore;
import com.kcom.vendingmachine.exceptions.InsufficientCoinageException;
import com.kcom.vendingmachine.exceptions.InvalidAmountException;

public class ChangeMakerImplTest {

  /* Optimal change tests */
  private ChangeMaker changeMaker;

  private Properties properties;

  @Before
  public void setup() {
    properties = Mockito.mock(Properties.class);
    changeMaker = new ChangeMakerImpl(new PropertiesCoinStore(properties));
  }

  @Test
  public void noOptimalChange() throws Exception {
    assertTrue(changeMaker.getOptimalChangeFor(0).isEmpty());
  }

  @Test
  public void negativeOptimalChange() throws Exception {
    Collection<Coin> coins = changeMaker.getOptimalChangeFor(-1);
    assertTrue(coins.isEmpty());
  }

  @Test
  public void maxNegativeOptimalChange() throws Exception {
    Collection<Coin> coins = changeMaker.getOptimalChangeFor(Integer.MIN_VALUE);
    assertTrue(coins.isEmpty());
  }

  @Test
  public void minOptimalChange() throws Exception {
    Collection<Coin> coins = changeMaker.getOptimalChangeFor(1);
    assertEquals(1, coins.size());
    assertEquals(1, coins.stream().filter(x -> x.getDenomination() == 1)
        .findFirst().get().getAmountOfCoins());
  }

  @Test
  public void maxOptimalChange() throws Exception {
    Collection<Coin> coins = changeMaker.getOptimalChangeFor(Integer.MAX_VALUE);
    assertEquals(4, coins.size());
    assertEquals(21474836, findAmount(coins, 100));
    assertEquals(2, findAmount(coins, 20));
    assertEquals(1, findAmount(coins, 2));
    assertEquals(1, findAmount(coins, 5));
  }

  @Test
  public void AllCoinageUsedOptimalChange() throws Exception {
    Collection<Coin> coins = changeMaker.getOptimalChangeFor(100 + 50 + 20 + 10
        + 5 + 2 + 1);

    assertEquals(7, coins.size());
    assertEquals(1, findAmount(coins, 100));
    assertEquals(1, findAmount(coins, 50));
    assertEquals(1, findAmount(coins, 20));
    assertEquals(1, findAmount(coins, 10));
    assertEquals(1, findAmount(coins, 5));
    assertEquals(1, findAmount(coins, 2));
    assertEquals(1, findAmount(coins, 1));
    assertEquals(1, findAmount(coins, 100));
  }

  // Not Optimal change

  @Test
  public void noChange() throws Exception {
    assertTrue(changeMaker.getChangeFor(0).isEmpty());
  }

  @Test
  public void negativeChange() throws Exception {
    Collection<Coin> coins = changeMaker.getChangeFor(-1);
    assertTrue(coins.isEmpty());
  }

  @Test
  public void maxNegativeChange() throws Exception {
    assertTrue(changeMaker.getChangeFor(Integer.MIN_VALUE).isEmpty());
  }

  @Test
  public void minChange() throws Exception {
    Mockito.when(properties.getProperty("1")).thenReturn("1");
    Collection<Coin> coins = changeMaker.getChangeFor(1);
    assertEquals(1, coins.size());
    assertEquals(1, findAmount(coins, 1));
  }

  @Test
  public void maxChangeMaxInteger() throws Exception {
    Mockito.when(properties.getProperty("100"))
        .thenReturn(Integer.toString(Integer.MAX_VALUE));
    Mockito.when(properties.getProperty("50")).thenReturn(Integer.toString(Integer.MAX_VALUE));
    Mockito.when(properties.getProperty("20")).thenReturn(Integer.toString(Integer.MAX_VALUE));
    Mockito.when(properties.getProperty("10")).thenReturn(Integer.toString(Integer.MAX_VALUE));
    Mockito.when(properties.getProperty("5")).thenReturn(Integer.toString(Integer.MAX_VALUE));
    Mockito.when(properties.getProperty("2")).thenReturn(Integer.toString(Integer.MAX_VALUE));
    Mockito.when(properties.getProperty("1")).thenReturn(Integer.toString(Integer.MAX_VALUE));
    Collection<Coin> coins = changeMaker.getChangeFor(Integer.MAX_VALUE);
    assertEquals(4, coins.size());
    assertEquals(21474836, findAmount(coins, 100));
    assertEquals(2, findAmount(coins, 20));
    assertEquals(1, findAmount(coins, 2));
    assertEquals(1, findAmount(coins, 5));
  }

  @Test
  public void maxChangeOnlyOnePences() throws Exception {
    Mockito.when(properties.getProperty("1")).thenReturn(
        Integer.toString(Integer.MAX_VALUE));
    Collection<Coin> coins = changeMaker.getChangeFor(Integer.MAX_VALUE);
    assertEquals(Integer.MAX_VALUE, findAmount(coins, 1));
  }

  @Test
  public void AllCoinageUsed() throws Exception {
    Mockito.when(properties.getProperty("100")).thenReturn(Integer.toString(1));
    Mockito.when(properties.getProperty("50")).thenReturn(Integer.toString(1));
    Mockito.when(properties.getProperty("20")).thenReturn(Integer.toString(1));
    Mockito.when(properties.getProperty("10")).thenReturn(Integer.toString(1));
    Mockito.when(properties.getProperty("5")).thenReturn(Integer.toString(1));
    Mockito.when(properties.getProperty("2")).thenReturn(Integer.toString(1));
    Mockito.when(properties.getProperty("1")).thenReturn(Integer.toString(1));
    Collection<Coin> coins = changeMaker.getChangeFor(100 + 50 + 20 + 10 + 5
        + 2 + 1);
    assertEquals(7, coins.size());
    assertEquals(1, findAmount(coins, 100));
    assertEquals(1, findAmount(coins, 50));
    assertEquals(1, findAmount(coins, 20));
    assertEquals(1, findAmount(coins, 10));
    assertEquals(1, findAmount(coins, 5));
    assertEquals(1, findAmount(coins, 2));
    assertEquals(1, findAmount(coins, 1));

  }

  @Test(expected = InsufficientCoinageException.class)
  public void noChangeInStore() throws Exception {
    changeMaker.getChangeFor(1);
  }

  @Test(expected = InsufficientCoinageException.class)
  public void insufficientCoinageByOne() throws Exception {
    Mockito.when(properties.getProperty("100")).thenReturn(Integer.toString(1));
    Mockito.when(properties.getProperty("50")).thenReturn(Integer.toString(1));
    Mockito.when(properties.getProperty("20")).thenReturn(Integer.toString(1));
    Mockito.when(properties.getProperty("10")).thenReturn(Integer.toString(1));
    Mockito.when(properties.getProperty("5")).thenReturn(Integer.toString(1));
    Mockito.when(properties.getProperty("2")).thenReturn(Integer.toString(1));
    Mockito.when(properties.getProperty("1")).thenReturn(Integer.toString(0));
    changeMaker.getChangeFor(100 + 50 + 20 + 10 + 5 + 2 + 1);
  }

  @Test(expected = InsufficientCoinageException.class)
  public void insufficientCoinageByTwo() throws Exception {
    Mockito.when(properties.getProperty("100")).thenReturn(Integer.toString(1));
    Mockito.when(properties.getProperty("50")).thenReturn(Integer.toString(1));
    Mockito.when(properties.getProperty("20")).thenReturn(Integer.toString(1));
    Mockito.when(properties.getProperty("10")).thenReturn(Integer.toString(1));
    Mockito.when(properties.getProperty("5")).thenReturn(Integer.toString(1));
    Mockito.when(properties.getProperty("2")).thenReturn(Integer.toString(0));
    Mockito.when(properties.getProperty("1")).thenReturn(Integer.toString(1));
    changeMaker.getChangeFor(100 + 50 + 20 + 10 + 5 + 2 + 1);
  }

  @Test(expected = InsufficientCoinageException.class)
  public void insufficientCoinageByFive() throws Exception {
    Mockito.when(properties.getProperty("100")).thenReturn(Integer.toString(1));
    Mockito.when(properties.getProperty("50")).thenReturn(Integer.toString(1));
    Mockito.when(properties.getProperty("20")).thenReturn(Integer.toString(1));
    Mockito.when(properties.getProperty("10")).thenReturn(Integer.toString(1));
    Mockito.when(properties.getProperty("5")).thenReturn(Integer.toString(0));
    Mockito.when(properties.getProperty("2")).thenReturn(Integer.toString(1));
    Mockito.when(properties.getProperty("1")).thenReturn(Integer.toString(1));
    changeMaker.getChangeFor(100 + 50 + 20 + 10 + 5 + 2 + 1);
  }

  @Test(expected = InsufficientCoinageException.class)
  public void insufficientCoinageByTen() throws Exception {
    Mockito.when(properties.getProperty("100")).thenReturn(Integer.toString(1));
    Mockito.when(properties.getProperty("50")).thenReturn(Integer.toString(1));
    Mockito.when(properties.getProperty("20")).thenReturn(Integer.toString(1));
    Mockito.when(properties.getProperty("10")).thenReturn(Integer.toString(0));
    Mockito.when(properties.getProperty("5")).thenReturn(Integer.toString(1));
    Mockito.when(properties.getProperty("2")).thenReturn(Integer.toString(1));
    Mockito.when(properties.getProperty("1")).thenReturn(Integer.toString(1));
    changeMaker.getChangeFor(100 + 50 + 20 + 10 + 5 + 2 + 1);
  }

  @Test(expected = InsufficientCoinageException.class)
  public void insufficientCoinageByTwenty() throws Exception {
    Mockito.when(properties.getProperty("100")).thenReturn(Integer.toString(1));
    Mockito.when(properties.getProperty("50")).thenReturn(Integer.toString(1));
    Mockito.when(properties.getProperty("20")).thenReturn(Integer.toString(0));
    Mockito.when(properties.getProperty("10")).thenReturn(Integer.toString(1));
    Mockito.when(properties.getProperty("5")).thenReturn(Integer.toString(1));
    Mockito.when(properties.getProperty("2")).thenReturn(Integer.toString(1));
    Mockito.when(properties.getProperty("1")).thenReturn(Integer.toString(1));
    changeMaker.getChangeFor(100 + 50 + 20 + 10 + 5 + 2 + 1);
  }

  @Test(expected = InsufficientCoinageException.class)
  public void insufficientCoinageByFifty() throws Exception {
    Mockito.when(properties.getProperty("100")).thenReturn(Integer.toString(1));
    Mockito.when(properties.getProperty("50")).thenReturn(Integer.toString(0));
    Mockito.when(properties.getProperty("20")).thenReturn(Integer.toString(1));
    Mockito.when(properties.getProperty("10")).thenReturn(Integer.toString(1));
    Mockito.when(properties.getProperty("5")).thenReturn(Integer.toString(1));
    Mockito.when(properties.getProperty("2")).thenReturn(Integer.toString(1));
    Mockito.when(properties.getProperty("1")).thenReturn(Integer.toString(1));
    changeMaker.getChangeFor(100 + 50 + 20 + 10 + 5 + 2 + 1);
  }

  @Test(expected = InsufficientCoinageException.class)
  public void insufficientCoinageByPound() throws Exception {
    Mockito.when(properties.getProperty("100")).thenReturn(Integer.toString(0));
    Mockito.when(properties.getProperty("50")).thenReturn(Integer.toString(1));
    Mockito.when(properties.getProperty("20")).thenReturn(Integer.toString(1));
    Mockito.when(properties.getProperty("10")).thenReturn(Integer.toString(1));
    Mockito.when(properties.getProperty("5")).thenReturn(Integer.toString(1));
    Mockito.when(properties.getProperty("2")).thenReturn(Integer.toString(1));
    Mockito.when(properties.getProperty("1")).thenReturn(Integer.toString(1));
    changeMaker.getChangeFor(100 + 50 + 20 + 10 + 5 + 2 + 1);
  }

  @Test(expected = InsufficientCoinageException.class)
  public void insufficientCoinageByMultiCoin() throws Exception {
    // Has enough money, just not the right coinage
    Mockito.when(properties.getProperty("100"))
        .thenReturn(Integer.toString(23));
    Mockito.when(properties.getProperty("50")).thenReturn(Integer.toString(1));
    Mockito.when(properties.getProperty("20")).thenReturn(Integer.toString(1));
    Mockito.when(properties.getProperty("10")).thenReturn(Integer.toString(1));
    Mockito.when(properties.getProperty("5")).thenReturn(Integer.toString(0));
    Mockito.when(properties.getProperty("2")).thenReturn(Integer.toString(0));
    Mockito.when(properties.getProperty("1")).thenReturn(Integer.toString(0));
    changeMaker.getChangeFor(100 + 50 + 20 + 10 + 5 + 2 + 1);
  }

  @Test
  public void randomValueOne() throws Exception {
    testWithExampleValues();
    Collection<Coin> coins = changeMaker.getChangeFor(1768);
    assertEquals(6, coins.size());
    assertEquals(11, findAmount(coins, 100));
    assertEquals(13, findAmount(coins, 50));
    assertEquals(1, findAmount(coins, 10));
    assertEquals(1, findAmount(coins, 5));
    assertEquals(1, findAmount(coins, 2));
    assertEquals(1, findAmount(coins, 1));
  }

  @Test
  public void randomValueTwo() throws Exception {
    testWithExampleValues();
    Collection<Coin> coins = changeMaker.getChangeFor(3775);
    assertEquals(4, coins.size());
    assertEquals(11, findAmount(coins, 100));
    assertEquals(24, findAmount(coins, 50));
    assertEquals(99, findAmount(coins, 10));
    assertEquals(97, findAmount(coins, 5));
  }

  @Test
  public void ensureStoreIsUpdated() throws Exception {
    Properties properties = new Properties();
    properties.put("100", "30");
    properties.put("50", "30");
    properties.put("20", "30");
    properties.put("10", "30");
    properties.put("5", "30");
    properties.put("2", "30");
    properties.put("1", "30");

    ChangeMaker localChangeMaker = new ChangeMakerImpl(new PropertiesCoinStore(
        properties));
    Collection<Coin> coins = localChangeMaker.getChangeFor(1168);


    assertEquals(6, coins.size());
    assertEquals(11, findAmount(coins, 100));
    assertEquals(1, findAmount(coins, 50));
    assertEquals(1, findAmount(coins, 10));
    assertEquals(1, findAmount(coins, 5));
    assertEquals(1, findAmount(coins, 2));
    assertEquals(1, findAmount(coins, 1));

    assertEquals("19", properties.getProperty("100"));
    assertEquals("29", properties.getProperty("50"));
    assertEquals("30", properties.getProperty("20"));
    assertEquals("29", properties.getProperty("10"));
    assertEquals("29", properties.getProperty("5"));
    assertEquals("29", properties.getProperty("2"));
    assertEquals("29", properties.getProperty("1"));
  }

  private void testWithExampleValues() {
    Mockito.when(properties.getProperty("100"))
        .thenReturn(Integer.toString(11));
    Mockito.when(properties.getProperty("50")).thenReturn(Integer.toString(24));
    Mockito.when(properties.getProperty("20")).thenReturn(Integer.toString(0));
    Mockito.when(properties.getProperty("10")).thenReturn(Integer.toString(99));
    Mockito.when(properties.getProperty("5")).thenReturn(Integer.toString(200));
    Mockito.when(properties.getProperty("2")).thenReturn(Integer.toString(11));
    Mockito.when(properties.getProperty("1")).thenReturn(Integer.toString(23));
  }

  private int findAmount(Collection<Coin> coins, int denomination) {
    return coins.stream().filter(x -> x.getDenomination() == denomination)
        .findFirst().get().getAmountOfCoins();
  }
}
